# Install docker-ce

This example follows https://docs.docker.com/engine/install/centos/
to install Docker.

Note that the default `full` base image includes podman; you
can instead install the `podman-docker` package for CLI compatibility
with docker.
