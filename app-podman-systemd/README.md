# Include a webserver container run via podman-systemd

This is a very simple image that runs a webserver (caddy)
as a "referenced" container image via
[podman-systemd](https://docs.podman.io/en/latest/markdown/podman-systemd.unit.5.html).
that is also configured for automatic updates.

